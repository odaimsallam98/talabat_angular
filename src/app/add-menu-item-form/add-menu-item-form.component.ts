import { Component, OnInit } from '@angular/core';
import { Form, NgForm } from '@angular/forms';
import { MenuItemsDataService } from '../menu-items-data.service';
import { MenuItem } from '../MenuItem';
import { Restaurant } from '../restaurant';
import { RestaurantsDataService } from '../restaurants-data.service';
import {FormsModule} from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
@Component({
  selector: 'app-add-menu-item-form',
  templateUrl: './add-menu-item-form.component.html',
  styleUrls: ['./add-menu-item-form.component.css']
})
export class AddMenuItemFormComponent implements OnInit {

 _id:number;
  constructor(private MenuItemsService:MenuItemsDataService, private route:ActivatedRoute, private _router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe(
        (params: Params) => {this._id = +params['resid'];}
      );
  }

  addMenuItem(f:NgForm)
  {
    this.MenuItemsService.addMenuItem(new MenuItem(f.value.MIName,f.value.price,f.value.description,f.value.image,1000,this._id));
    console.log("from add menu item comp, add executed" + f.value.MIName + f.value.price + f.value.description + f.value.image + this._id)  
    this._router.navigateByUrl(`list_menu_items/${this._id}`);
    console.log("from add menu item navigated to list of items of res with id" +this._id)
  }
}
